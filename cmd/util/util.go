package util

import (
	"os"
)

// GetEnv get value from os environment, if not available, then return fallback.
func GetEnv(key, fallback string) string {
	value, exists := os.LookupEnv(key)
	if exists && len(value) != 0 {
		return value
	}
	return fallback
}
