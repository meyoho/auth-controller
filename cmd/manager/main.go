/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"os"

	"alauda.io/auth-controller/cmd/util"
	"alauda.io/auth-controller/pkg/apis"
	"alauda.io/auth-controller/pkg/apis/auth/v1alpha1"
	"alauda.io/auth-controller/pkg/controller"
	"alauda.io/auth-controller/pkg/controller/clusterrole"
	"alauda.io/auth-controller/pkg/controller/project"

	"k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/runtime/signals"
)

func main() {
	logf.SetLogger(logf.ZapLogger(false))
	log := logf.Log.WithName("auth-controller")

	// Get a config to talk to the apiserver
	log.Info("setting up client for manager")
	cfg, err := config.GetConfig()
	if err != nil {
		log.Error(err, "unable to set up client config")
		os.Exit(1)
	}

	// Create a new Cmd to provide shared dependencies and start components
	leaderElectionNamespace := util.GetEnv("LEADER_ELECTION_NAMESPACE", v1alpha1.NamespaceAlaudaSystem)
	log.Info("setting up manager", "leader election namespace", leaderElectionNamespace)
	mgr, err := manager.New(cfg, manager.Options{
		LeaderElection:          true,
		LeaderElectionID:        "auth-controller-lock",
		LeaderElectionNamespace: leaderElectionNamespace,
	})

	if err != nil {
		log.Error(err, "unable to set up overall controller manager")
		os.Exit(1)
	}

	// registry scheme
	v1beta1.AddToScheme(mgr.GetScheme())

	// Note: if kind is a CRD, it should be installed before calling Start!
	// init the project
	if err := project.InitDefaultProject(mgr.GetClient()); err != nil {
		log.Error(err, "Init project error")
	}

	// init the default ClusterRoles
	mgr.Add(manager.RunnableFunc(func(s <-chan struct{}) error {
		if err := clusterrole.InitDefaultClusterRoles(mgr.GetClient()); err != nil {
			log.Error(err, "Init default clusterroles error")
		}
		<-s
		return err
	}))

	log.Info("Registering Components.")

	// Setup Scheme for all resources
	log.Info("setting up scheme")
	if err := apis.AddToScheme(mgr.GetScheme()); err != nil {
		log.Error(err, "unable add APIs to scheme")
		os.Exit(1)
	}

	// Setup all Controllers
	log.Info("Setting up controller")
	if err := controller.AddToManager(mgr); err != nil {
		log.Error(err, "unable to register controllers to the manager")
		os.Exit(1)
	}

	// Start the Cmd
	log.Info("Starting the Cmd.")
	if err := mgr.Start(signals.SetupSignalHandler()); err != nil {
		log.Error(err, "unable to run the manager")
		os.Exit(1)
	}
}
