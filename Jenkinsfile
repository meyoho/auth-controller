// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd') _

// image can be used for promoting...
def IMAGE
def DEBUG = false
def deployment
def RELEASE_VERSION
def RELEASE_BUILD
def release
def PROXY_CREDENTIALS
def FOLDER = "src/alauda.io/auth-controller"
pipeline {
	agent { label 'golang-1.11' }

	options {
		buildDiscarder(logRotator(numToKeepStr: '10'))
		disableConcurrentBuilds()
		// skipDefaultCheckout()
	}

	environment {
		FOLDER = 'src/alauda.io/auth-controller'
		GOPATH = "${WORKSPACE}"

		// for building an scanning
		REPOSITORY = "auth-controller"
		OWNER = "mathildetech"
		// sonar feedback user
		// needs to change together with the credentialsID
		BITBUCKET_FEEDBACK_ACCOUNT = "alaudabot"
		SONARQUBE_BITBUCKET_CREDENTIALS = "alaudabot"
		NAMESPACE = "alauda-system"
		DEPLOYMENT_NAME = "auth-controller"
		CONTAINER = "manager"
		DINGDING_BOT = "auth-bot"
		TAG_CREDENTIALS = "alaudabot-bitbucket"
        PROXY_CREDENTIALS_ID = 'proxy'

		IMAGE_REPOSITORY = "index.alauda.cn/alaudak8s/auth-controller"
		IMAGE_TEST_REPOSITORY = "index.alauda.cn/alaudak8s/devops-apiserver-tests"
		IMAGE_CREDENTIALS = "alaudak8s"
	}

	stages {
		stage('Checkout') {
			steps {
				script {
					dir(FOLDER) {
						container('tools') {
							// checkout code
							withCredentials([
								usernamePassword(credentialsId: PROXY_CREDENTIALS_ID, passwordVariable: 'PROXY_ADDRESS', usernameVariable: 'PROXY_ADDRESS_PASS')
							]) { PROXY_CREDENTIALS = "${PROXY_ADDRESS}" }
							sh "git config --global http.proxy ${PROXY_CREDENTIALS}"
							def scmVars
							retry(2) { scmVars = checkout scm }
							release = deploy.release(scmVars)

							RELEASE_BUILD = release.version
							RELEASE_VERSION = release.majorVersion
							// echo "release ${RELEASE_VERSION} - release build ${RELEASE_BUILD}"
							echo """
								release ${RELEASE_VERSION}
								version ${release.version}
								is_release ${release.is_release}
								is_build ${release.is_build}
								is_master ${release.is_master}
								deploy_env ${release.environment}
								auto_test ${release.auto_test}
								environment ${release.environment}
								majorVersion ${release.majorVersion}
							"""
							// copying kubectl from tools
							sh "cp /usr/local/bin/kubectl ."
						}
					}
				}
			}
		}
		stage('CI'){
			failFast true
			parallel {
				stage('Build') {
					steps {
						script {
							dir(FOLDER) {
								container('golang') { sh "CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o ./bin/auth-controller alauda.io/auth-controller/cmd/manager" }
								container('tools') {
									sh "upx ./bin/auth-controller || true"
									sh "cp config/images/Dockerfile ./bin/"
									// currently is building code inside the container
									IMAGE = deploy.dockerBuild(
										"bin/Dockerfile", //Dockerfile
										"bin", // build context
										IMAGE_REPOSITORY, // repo address
										RELEASE_BUILD, // tag
										IMAGE_CREDENTIALS, // credentials for pushing
										).setArg("commit_id", "${release.commit}").setArg("app_version", "${RELEASE_BUILD}")
									// start and push
									IMAGE.start().push()
								}
							}
						}

					}
				}
				stage('Code Scan') {
					steps {
						script {
							dir(FOLDER) {
								sh "ls -lah && cat sonar-project.properties"
								sh "echo 'sonar.projectVersion=${RELEASE_BUILD}' >> sonar-project.properties"
								container('golang') { sh "make test" }
								container('tools') {
									deploy.scan(
										REPOSITORY,
										release.branch,
										SONARQUBE_BITBUCKET_CREDENTIALS,
										".",
										DEBUG,
										OWNER,
										BITBUCKET_FEEDBACK_ACCOUNT
										).start()
								}
							}
						}
					}
				}
			}
		}
		// after build it should start deploying
		stage('Deploying') {
			when {
				expression {
					release.auto_test && !params.DEBUG
				}
			}
			steps {
				script {
					dir(FOLDER) {
						container('tools') {
							release.setupTestEnv()

							deployment = deploy.getDeploymentObj(NAMESPACE, DEPLOYMENT_NAME)
							deployment.saveState()
							deployment.setImage(IMAGE.getImage(), "auth-controller-manager")
							deployment.apply()
						}
					}
				}
			}
		}
		stage('Promoting') {
			// limit this stage to master only
			when {
				expression {
					release.auto_test && release.deploy_env != '' && !params.DEBUG
				}
			}
			steps {
				script {
					dir(FOLDER) {
						container('tools') {
							release.setupDeployEnv()

							deployment = deploy.getDeploymentObj(NAMESPACE, DEPLOYMENT_NAME)
							deployment.saveState()
							deployment.setImage(IMAGE.getImage(), "auth-controller-manager")
							deployment.apply()
						}
					}
				}
			}
		}
		stage('Tag git') {
			when {
				expression {
					release.tag && !params.DEBUG
				}
			}
			steps {
				script {
					dir(FOLDER) {
						container('tools') {
							deploy.gitTag(
								TAG_CREDENTIALS,
								RELEASE_BUILD,
								OWNER,
								REPOSITORY
								)
						}
					}
				}
			}
		}
		stage('Chart Update') {
			when {
				expression {
					release.chartBranch != '' && !params.DEBUG
				}
			}
			steps {
				script {
					echo "will trigger charts-pipeline using branch ${release.chartBranch}"
					build job: '/charts-pipeline', parameters: [
						[$class: 'StringParameterValue', name: 'CHART', value: 'portal'],
						[$class: 'StringParameterValue', name: 'VERSION', value: ''],
						[$class: 'StringParameterValue', name: 'COMPONENT', value: 'auth-controller'],
						[$class: 'StringParameterValue', name: 'IMAGE_TAG', value: RELEASE_BUILD],
						[$class: 'BooleanParameterValue', name: 'DEBUG', value: false],
						[$class: 'StringParameterValue', name: 'ENV', value: ''],
						[$class: 'StringParameterValue', name: 'BRANCH', value: release.chartBranch],
					], wait: false
				}
			}
		}

	}

	post {
		success {
			dir(FOLDER) {
				script {
					container('tools') {
						def msg = "流水线完成了"
						if (release.is_master) {
							msg = "上线啦！"
						}
						deploy.notificationSuccess(DEPLOYMENT_NAME, DINGDING_BOT, msg, RELEASE_BUILD)
						if (release != null) { release.cleanEnv() }
					}
				}
			}
		}
		failure {
			dir(FOLDER) {
				script {
					container('tools') {
						if (release != null) {
							release.setupTestEnv()
							if (deployment != null) {
								deployment.rollbackConfig().apply()
							}
						}
						deploy.notificationFailed(DEPLOYMENT_NAME, DINGDING_BOT, "流水线失败了", RELEASE_BUILD)
						if (release != null) { release.cleanEnv() }
					}
				}
			}
		}
	}
}

