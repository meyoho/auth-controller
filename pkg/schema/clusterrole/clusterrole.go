package clusterrole

import (
	"alauda.io/auth-controller/pkg/constant/clusterrole"
	testv1 "k8s.io/api/rbac/v1"
)

var DefaultClusterRoles = map[string]*testv1.ClusterRole{
	clusterrole.SUPER_ADMIN:         SchemaSuperAdmin,
	clusterrole.ADMIN:               SchemaAdmin,
	clusterrole.UNTRUSTED_ADMIN:     SchemaUntrustedAdmin,
	clusterrole.PLATFORM_AUDITOR:    SchemaPlatformAuditor,
	clusterrole.PROJECT_ADMIN:       SchemaProjectAdmin,
	clusterrole.PROJECT_AUDITOR:     SchemaProjectAuditor,
	clusterrole.NAMESPACE_ADMIN:     SchemaNamespaceAdmin,
	clusterrole.NAMESPACE_DEVELOPER: SchemaNamespaceDeveloper,
	clusterrole.NAMESPACE_AUDITOR:   SchemaNamespaceAuditor,

	clusterrole.EXT_PROJECT_ADMIN:       SchemaExtProjectAdmin,
	clusterrole.EXT_PROJECT_AUDITOR:     SchemaExtProjectAuditor,
	clusterrole.EXT_NAMESPACE_DEVELOPER: SchemaExtNamespaceDeveloper,
	clusterrole.EXT_NAMESPACE_ADMIN:     SchemaExtNamespaceAdmin,
	clusterrole.EXT_NAMESPACE_AUDITOR:   SchemaExtNamespaceAuditor,
}

var AllPortalProducts = []string{
	"acp-devops", "acp-machine-learning", "acp-service-framework", "acp-service-mesh",
	"alauda-kubernetes", "bitbucket", "chandao", "confluence", "elastic", "github",
	"gitlab", "grafana", "jira", "mayun", "prometheus", "sonarqube", "trello",
}

var OtherPortalProducts = []string{
	"acp-devops", "acp-machine-learning", "acp-service-framework", "acp-service-mesh",
	"bitbucket", "chandao", "confluence", "github",
	"gitlab", "jira", "mayun", "sonarqube", "trello",
}
