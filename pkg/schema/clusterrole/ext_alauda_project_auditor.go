package clusterrole

import (
	constClusterrole "alauda.io/auth-controller/pkg/constant/clusterrole"
	"alauda.io/auth-controller/pkg/constant/groups"
	testv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// SchemaExtProjectAuditor is a extended cluster role for project auditor
var SchemaExtProjectAuditor = &testv1.ClusterRole{
	TypeMeta: metav1.TypeMeta{
		Kind:       "ClusterRole",
		APIVersion: "rbac.authorization.k8s.io/v1",
	},
	ObjectMeta: metav1.ObjectMeta{
		Name: constClusterrole.EXT_PROJECT_AUDITOR,
	},
	Rules: []testv1.PolicyRule{
		{
			APIGroups: []string{
				"",
			},
			Resources: []string{
				"namespaces",
				"events",
			},
			Verbs: []string{
				// "get",
				"list",
			},
		},
		{
			APIGroups: []string{
				"route.openshift.io",
			},
			Resources: []string{
				"routes",
			},
			Verbs: []string{
				"get",
				"list",
			},
		},
		// add list storageclass permission
		extListStorageClassPermission,
		// add list cluster roles
		extListGetWatchClusterRolePermission,
		// portal start
		{
			APIGroups: []string{
				"",
			},
			Resources: []string{
				"configmaps",
			},
			ResourceNames: []string{
				"portal-configmap",
			},
			Verbs: []string{
				"get",
			},
		},
		{
			APIGroups: []string{"portal.alauda.io"},
			Resources: []string{"alaudaproducts"},
			Verbs:     []string{"list"},
		},
		{
			APIGroups:     []string{"portal.alauda.io"},
			Resources:     []string{"alaudaproducts"},
			Verbs:         []string{"get"},
			ResourceNames: OtherPortalProducts,
		},
		// portal end
		// devops start
		// project: change in the future
		extListProjectsPermission,
		extListAuthProjectsPermission,
		extListGetWatchDevopsPermission,
		extListGetWatchAuthPermission,
		extListGetWatchCatalogPermission,
		extListGetListWatchEventsPermission,
		// devops end

		// asf
		{
			APIGroups: []string{groups.ASFAPIGroup},
			Resources: []string{"*"},
			Verbs:     []string{"get", "list", "watch"},
		},
		// asm start
		extListGetWatchAsmClusterConfigPermission,
		// asm end
	},
}
