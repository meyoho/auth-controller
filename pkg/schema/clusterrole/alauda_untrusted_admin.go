package clusterrole

import (
	constClusterrole "alauda.io/auth-controller/pkg/constant/clusterrole"
	"alauda.io/auth-controller/pkg/constant/groups"
	testv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var SchemaUntrustedAdmin = &testv1.ClusterRole{
	TypeMeta: metav1.TypeMeta{
		Kind:       "ClusterRole",
		APIVersion: "rbac.authorization.k8s.io/v1",
	},
	ObjectMeta: metav1.ObjectMeta{
		Name: constClusterrole.UNTRUSTED_ADMIN,
		Labels: map[string]string{
			"alauda.io/bootstrapping": "rbac-defaults",
			"alauda.io/role-level":    "platform",
			"alauda.io/is-shown":      "false",
		},
		Annotations: map[string]string{
			"alauda.io/role-name-zh": "平台非信任管理员",
			"alauda.io/role-name-en": "Untrusted Admin",
		},
	},
	Rules: []testv1.PolicyRule{
		{
			APIGroups: []string{
				"",
			},
			Resources: []string{
				"nodes",
				"pods",
				"pods/attach",
				"pods/exec",
				"pods/portforward",
				"pods/proxy",
				"configmaps",
				"endpoints",
				"persistentvolumeclaims",
				"replicationcontrollers",
				"replicationcontrollers/scale",
				"secrets",
				"serviceaccounts",
				"services",
				"services/proxy",
				"namespaces",
				"bindings",
				"events",
				"limitranges",
				"namespaces/status",
				"pods/log",
				"pods/status",
				"replicationcontrollers/status",
				"resourcequotas",
				"resourcequotas/status",
			},
			Verbs: []string{
				"*",
			},
		},
		{
			APIGroups: []string{
				"",
			},
			Resources: []string{
				"serviceaccounts",
			},
			Verbs: []string{
				"impersonate",
			},
		},
		{
			APIGroups: []string{
				"apps",
			},
			Resources: []string{
				"daemonsets",
				"deployments",
				"deployments/rollback",
				"deployments/scale",
				"replicasets",
				"replicasets/scale",
				"statefulsets",
			},
			Verbs: []string{
				"*",
			},
		},
		{
			APIGroups: []string{
				"autoscaling",
			},
			Resources: []string{
				"horizontalpodautoscalers",
			},
			Verbs: []string{
				"*",
			},
		},
		{
			APIGroups: []string{
				"batch",
			},
			Resources: []string{
				"cronjobs",
				"jobs",
			},
			Verbs: []string{
				"*",
			},
		},
		{
			APIGroups: []string{
				"extensions",
			},
			Resources: []string{
				"daemonsets",
				"deployments",
				"deployments/rollback",
				"deployments/scale",
				"ingresses",
				"replicasets",
				"replicasets/scale",
				"replicationcontrollers/scale",
			},
			Verbs: []string{
				"*",
			},
		},
		{
			APIGroups: []string{
				"policy",
			},
			Resources: []string{
				"poddisruptionbudgets",
			},
			Verbs: []string{
				"*",
			},
		},
		{
			APIGroups: []string{
				"authorization.k8s.io",
			},
			Resources: []string{
				"localsubjectaccessreviews",
			},
			Verbs: []string{
				"*",
			},
		},
		{
			APIGroups: []string{
				groups.DevOpsAPIGroup,
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"create",
				"get",
				"list",
				"patch",
				"update",
				"watch",
			},
		},
		{
			APIGroups: []string{
				groups.AUTH_APIGROUP,
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"create",
				"get",
				"list",
				"patch",
				"update",
				"watch",
			},
		},
		{
			APIGroups: []string{
				groups.AppAPIGroup,
				groups.AUTH_APIGROUP,
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"create",
				"get",
				"list",
				"patch",
				"update",
				"watch",
			},
		},
		{
			APIGroups: []string{
				groups.CatalogAPIGroup,
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"create",
				"get",
				"list",
				"patch",
				"update",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"rbac.authorization.k8s.io",
			},
			Resources: []string{
				"clusterrolebindings",
				"clusterroles",
				"rolebindings",
				"roles",
			},
			Verbs: []string{
				"*",
			},
		},
		{
			APIGroups: []string{
				"route.openshift.io",
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"*",
			},
		},
		// portal start
		{
			APIGroups: []string{"portal.alauda.io"},
			Resources: []string{"alaudaproducts"},
			Verbs:     []string{"list"},
		},
		{
			APIGroups:     []string{"portal.alauda.io"},
			Resources:     []string{"alaudaproducts"},
			Verbs:         []string{"get"},
			ResourceNames: AllPortalProducts,
		},
		// portal end
		allVerbsAllResourcesDevopsPermission,
		//auth
		allVerbsAllResourcesAuthPermission,
		// catalog
		allVerbsReleasesCatalogPermission,
		listGetWatchCatalogDomainPermission,
		allVerbsApplicationsAppPermission,
		// devops end

		{
			APIGroups: []string{
				groups.ASFAPIGroup,
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"*",
			},
		},
		// istio
		allVerbsIstioNetworkPermission,
		allVerbsIstioAuthPermission,
	},
}
