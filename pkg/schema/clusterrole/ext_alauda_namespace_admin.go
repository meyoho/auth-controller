package clusterrole

import (
	constClusterrole "alauda.io/auth-controller/pkg/constant/clusterrole"
	"alauda.io/auth-controller/pkg/constant/groups"
	testv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// SchemaExtNamespaceAdmin is a extended cluster role for namespace admin
var SchemaExtNamespaceAdmin = &testv1.ClusterRole{
	TypeMeta: metav1.TypeMeta{
		Kind:       "ClusterRole",
		APIVersion: "rbac.authorization.k8s.io/v1",
	},
	ObjectMeta: metav1.ObjectMeta{
		Name: constClusterrole.EXT_NAMESPACE_ADMIN,
	},
	Rules: []testv1.PolicyRule{
		{
			APIGroups: []string{
				"",
			},
			Resources: []string{
				"namespaces",
				"events",
			},
			Verbs: []string{
				"get",
				"list",
			},
		},
		{
			APIGroups: []string{
				"route.openshift.io",
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"*",
			},
		},
		{
			APIGroups: []string{
				"authorization.openshift.io",
				"rbac.authorization.k8s.io",
			},
			Resources: []string{
				"clusterrolebindings",
				"rolebindings",
			},
			Verbs: []string{
				"*",
			},
		},
		// portal start
		{
			APIGroups: []string{
				"",
			},
			Resources: []string{
				"configmaps",
			},
			ResourceNames: []string{
				"portal-configmap",
			},
			Verbs: []string{
				"get",
			},
		},
		{
			APIGroups: []string{"portal.alauda.io"},
			Resources: []string{"alaudaproducts"},
			Verbs:     []string{"list"},
		},
		{
			APIGroups:     []string{"portal.alauda.io"},
			Resources:     []string{"alaudaproducts"},
			Verbs:         []string{"get"},
			ResourceNames: OtherPortalProducts,
		},
		extListProjectsPermission,
		extListAuthProjectsPermission,
		extListGetWatchCatalogPermission,
		extListGetListWatchEventsPermission,
		extListGetWatchDevopsPermission,
		extListGetWatchAuthPermission,
		extListStorageClassPermission,
		extListGetWatchClusterRolePermission,
		{
			APIGroups: []string{
				groups.DevOpsAPIGroup,
			},
			Resources: []string{
				"clusterpipelinetemplates",
				"clusterpipelinetasktemplates",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		{
			APIGroups: []string{
				groups.AUTH_APIGROUP,
			},
			Resources: []string{
				"clusterpipelinetemplates",
				"clusterpipelinetasktemplates",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		{
			APIGroups: []string{
				groups.DevOpsAPIGroup,
			},
			Resources: []string{
				"clusterpipelinetemplates/preview",
			},
			Verbs: []string{
				"get",
				"create",
			},
		},
		{
			APIGroups: []string{
				groups.AUTH_APIGROUP,
			},
			Resources: []string{
				"clusterpipelinetemplates/preview",
			},
			Verbs: []string{
				"get",
				"create",
			},
		},
		{
			APIGroups: []string{
				groups.ASFAPIGroup,
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"*",
			},
		},
		// asm start
		extListGetWatchAsmClusterConfigPermission,
		// asm end
	},
}
