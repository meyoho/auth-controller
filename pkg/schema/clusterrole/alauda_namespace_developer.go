package clusterrole

import (
	constClusterrole "alauda.io/auth-controller/pkg/constant/clusterrole"
	"alauda.io/auth-controller/pkg/constant/groups"
	testv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var SchemaNamespaceDeveloper = &testv1.ClusterRole{
	TypeMeta: metav1.TypeMeta{
		Kind:       "ClusterRole",
		APIVersion: "rbac.authorization.k8s.io/v1",
	},
	ObjectMeta: metav1.ObjectMeta{
		Name: constClusterrole.NAMESPACE_DEVELOPER,
		Labels: map[string]string{
			"alauda.io/bootstrapping": "rbac-defaults",
			"alauda.io/role-level":    "namespace",
			"alauda.io/is-shown":      "true",
		},
		Annotations: map[string]string{
			"alauda.io/role-name-zh": "项目成员",
			"alauda.io/role-name-en": "Namespace Developer",
		},
	},
	Rules: []testv1.PolicyRule{
		{
			APIGroups: []string{
				"",
			},
			Resources: []string{
				"pods",
				"pods/attach",
				"pods/exec",
				"pods/portforward",
				"pods/proxy",
				"configmaps",
				"endpoints",
				"persistentvolumeclaims",
				"replicationcontrollers",
				"replicationcontrollers/scale",
				"secrets",
				"serviceaccounts",
				"services",
				"services/proxy",
			},
			Verbs: []string{
				"create",
				"delete",
				"deletecollection",
				"get",
				"list",
				"patch",
				"update",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"",
			},
			Resources: []string{
				"namespaces",
				"bindings",
				"events",
				"limitranges",
				"namespaces/status",
				"pods/log",
				"pods/status",
				"replicationcontrollers/status",
				"resourcequotas",
				"resourcequotas/status",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"apps",
			},
			Resources: []string{
				"daemonsets",
				"deployments",
				"deployments/rollback",
				"deployments/scale",
				"replicasets",
				"replicasets/scale",
				"statefulsets",
			},
			Verbs: []string{
				"create",
				"delete",
				"deletecollection",
				"get",
				"list",
				"patch",
				"update",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"autoscaling",
			},
			Resources: []string{
				"horizontalpodautoscalers",
			},
			Verbs: []string{
				"create",
				"delete",
				"deletecollection",
				"get",
				"list",
				"patch",
				"update",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"batch",
			},
			Resources: []string{
				"cronjobs",
				"jobs",
			},
			Verbs: []string{
				"create",
				"delete",
				"deletecollection",
				"get",
				"list",
				"patch",
				"update",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"extensions",
			},
			Resources: []string{
				"daemonsets",
				"deployments",
				"deployments/rollback",
				"deployments/scale",
				"ingresses",
				"replicasets",
				"replicasets/scale",
				"replicationcontrollers/scale",
			},
			Verbs: []string{
				"create",
				"delete",
				"deletecollection",
				"get",
				"list",
				"patch",
				"update",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"policy",
			},
			Resources: []string{
				"poddisruptionbudgets",
			},
			Verbs: []string{
				"create",
				"delete",
				"deletecollection",
				"get",
				"list",
				"patch",
				"update",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"rbac.authorization.k8s.io",
			},
			Resources: []string{
				"rolebindings",
			},
			Verbs: []string{
				"list",
			},
		},
		{
			APIGroups: []string{
				"authorization.k8s.io",
			},
			Resources: []string{
				"localsubjectaccessreviews",
			},
			Verbs: []string{
				"create",
			},
		},
		{
			APIGroups: []string{
				"route.openshift.io",
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"*",
			},
		},
		// check shared_permissions.go
		listGetWatchDevopsPermission,
		listGetWatchAuthPermission,
		{
			APIGroups: []string{
				groups.DevOpsAPIGroup,
			},
			Resources: []string{
				"pipelines",
				"pipelines/logs",
				"pipelines/tasks",
				"pipelineconfigs",
				"pipelineconfigs/preview",
				"pipelinetemplates/preview",
				"imagerepositories/security",
			},
			Verbs: []string{
				"*",
			},
		},
		{
			APIGroups: []string{
				groups.AUTH_APIGROUP,
			},
			Resources: []string{
				"projects",
			},
			Verbs: []string{
				"*",
			},
		},
		// catalog
		allVerbsReleasesCatalogPermission,
		allVerbsApplicationsAppPermission,
		listGetWatchCatalogDomainPermission,
		// asf
		{
			APIGroups: []string{
				groups.ASFAPIGroup,
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"*",
			},
		},
		// istio
		allVerbsIstioNetworkPermission,
		allVerbsIstioAuthPermission,
	},
}
