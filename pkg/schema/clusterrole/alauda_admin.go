package clusterrole

import (
	constClusterrole "alauda.io/auth-controller/pkg/constant/clusterrole"
	testv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var SchemaAdmin = &testv1.ClusterRole{
	TypeMeta: metav1.TypeMeta{
		Kind:       "ClusterRole",
		APIVersion: "rbac.authorization.k8s.io/v1",
	},
	ObjectMeta: metav1.ObjectMeta{
		Name: constClusterrole.ADMIN,
		Labels: map[string]string{
			"alauda.io/bootstrapping": "rbac-defaults",
			"alauda.io/role-level":    "platform",
			"alauda.io/is-shown":      "true",
		},
		Annotations: map[string]string{
			"alauda.io/role-name-zh": "平台管理员",
			"alauda.io/role-name-en": "Admin",
		},
	},
	Rules: []testv1.PolicyRule{
		{
			APIGroups: []string{
				"*",
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"*",
			},
		},
		{
			NonResourceURLs: []string{
				"*",
			},
			Verbs: []string{
				"*",
			},
		},
	},
}
