package clusterrole

import (
	constClusterrole "alauda.io/auth-controller/pkg/constant/clusterrole"
	"alauda.io/auth-controller/pkg/constant/groups"
	testv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var SchemaPlatformAuditor = &testv1.ClusterRole{
	TypeMeta: metav1.TypeMeta{
		Kind:       "ClusterRole",
		APIVersion: "rbac.authorization.k8s.io/v1",
	},
	ObjectMeta: metav1.ObjectMeta{
		Name: constClusterrole.PLATFORM_AUDITOR,
		Labels: map[string]string{
			"alauda.io/bootstrapping": "rbac-defaults",
			"alauda.io/role-level":    "platform",
			"alauda.io/is-shown":      "false",
		},
		Annotations: map[string]string{
			"alauda.io/role-name-zh": "平台审计人员",
			"alauda.io/role-name-en": "Platform Auditor",
		},
	},
	Rules: []testv1.PolicyRule{
		{
			APIGroups: []string{
				"",
			},
			Resources: []string{
				"nodes",
				"pods",
				"pods/attach",
				"pods/exec",
				"pods/portforward",
				"pods/proxy",
				"configmaps",
				"endpoints",
				"persistentvolumeclaims",
				"replicationcontrollers",
				"replicationcontrollers/scale",
				"secrets",
				"serviceaccounts",
				"services",
				"services/proxy",
				"bindings",
				"events",
				"limitranges",
				"namespaces/status",
				"pods/log",
				"pods/status",
				"replicationcontrollers/status",
				"resourcequotas",
				"resourcequotas/status",
				"namespaces",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"apps",
			},
			Resources: []string{
				"daemonsets",
				"deployments",
				"deployments/rollback",
				"deployments/scale",
				"replicasets",
				"replicasets/scale",
				"statefulsets",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"autoscaling",
			},
			Resources: []string{
				"horizontalpodautoscalers",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"batch",
			},
			Resources: []string{
				"cronjobs",
				"jobs",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"extensions",
			},
			Resources: []string{
				"daemonsets",
				"deployments",
				"deployments/rollback",
				"deployments/scale",
				"ingresses",
				"replicasets",
				"replicasets/scale",
				"replicationcontrollers/scale",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		{
			APIGroups: []string{
				"policy",
			},
			Resources: []string{
				"poddisruptionbudgets",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		// check shared_permissions.go
		listGetWatchDevopsPermission,
		listGetWatchAuthPermission,
		listGetWatchCatalogPermission,
		listGetWatchApplicationsAppPermission,
		{
			APIGroups: []string{
				"rbac.authorization.k8s.io",
			},
			Resources: []string{
				"clusterrolebindings",
				"clusterroles",
				"rolebindings",
				"roles",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		// portal start
		{
			APIGroups: []string{"portal.alauda.io"},
			Resources: []string{"alaudaproducts"},
			Verbs:     []string{"list"},
		},
		{
			APIGroups:     []string{"portal.alauda.io"},
			Resources:     []string{"alaudaproducts"},
			Verbs:         []string{"get"},
			ResourceNames: AllPortalProducts,
		},
		// portal end

		//Asf
		{
			APIGroups: []string{
				groups.ASFAPIGroup,
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		listGetWatchIstioNetworkPermission,
		listGetWatchIstioAuthPermission,
	},
}
