package clusterrole

import (
	"alauda.io/auth-controller/pkg/constant/groups"
	testv1 "k8s.io/api/rbac/v1"
)

// project: change in the future
var extListProjectsPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.DevOpsAPIGroup,
	},
	Resources: []string{
		"projects",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var extListAuthProjectsPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.AUTH_APIGROUP,
	},
	Resources: []string{
		"projects",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var extListGetWatchDevopsPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.DevOpsAPIGroup,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var extListGetWatchAuthPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.AUTH_APIGROUP,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var extListGetWatchCatalogPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.CatalogAPIGroup,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var extListGetListWatchEventsPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.EventsAPIGroup,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var extListGetWatchClusterRolePermission = testv1.PolicyRule{
	APIGroups: []string{
		"rbac.authorization.k8s.io",
	},
	Resources: []string{
		"clusterroles",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var extListStorageClassPermission = testv1.PolicyRule{
	APIGroups: []string{
		"storage.k8s.io",
	},
	Resources: []string{
		"storageclasses",
	},
	Verbs: []string{
		"get",
		"list",
	},
}
var extListGetWatchAsmClusterConfigPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.AsmAPIGroup,
	},
	Resources: []string{
		"clusterconfigs",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}