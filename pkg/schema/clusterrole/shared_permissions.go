package clusterrole

import (
	"alauda.io/auth-controller/pkg/constant/groups"
	testv1 "k8s.io/api/rbac/v1"
)

// project: change in the future
var listProjectsPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.DevOpsAPIGroup,
	},
	Resources: []string{
		"projects",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}
var listGetWatchDevopsPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.DevOpsAPIGroup,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var listGetWatchAuthPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.AUTH_APIGROUP,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var allVerbsAllResourcesDevopsPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.DevOpsAPIGroup,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"*",
	},
}

var allVerbsAllResourcesAuthPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.AUTH_APIGROUP,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"*",
	},
}

var listGetWatchCatalogPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.CatalogAPIGroup,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var allVerbsAllResourcesCatalogPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.CatalogAPIGroup,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"*",
	},
}

var allVerbsApplicationsAppPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.AppAPIGroup,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"*",
	},
}

var listGetWatchApplicationsAppPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.AppAPIGroup,
	},
	Resources: []string{
		"*",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var allVerbsReleasesCatalogPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.CatalogAPIGroup,
	},
	Resources: []string{
		"releases",
	},
	Verbs: []string{
		"*",
	},
}

var listGetWatchCatalogDomainPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.CatalogAPIGroup,
	},
	Resources: []string{
		"charts",
		"domains",
		"domainbindings",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var listGetWatchIstioNetworkPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.IstioNetworkApiGroup,
	},
	Resources: []string{
		"virtualservices",
		"destinationrules",
		"gateways",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var listGetWatchIstioAuthPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.IstioAuthApiGroup,
	},
	Resources: []string{
		"policies",
	},
	Verbs: []string{
		"get",
		"list",
		"watch",
	},
}

var allVerbsIstioNetworkPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.IstioNetworkApiGroup,
	},
	Resources: []string{
		"virtualservices",
		"destinationrules",
		"gateways",
		"policies",
	},
	Verbs: []string{
		"*",
	},
}

var allVerbsIstioAuthPermission = testv1.PolicyRule{
	APIGroups: []string{
		groups.IstioAuthApiGroup,
	},
	Resources: []string{
		"policies",
	},
	Verbs: []string{
		"*",
	},
}
