package clusterrole

import (
	constClusterrole "alauda.io/auth-controller/pkg/constant/clusterrole"
	"alauda.io/auth-controller/pkg/constant/groups"
	testv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// SchemaExtNamespaceDeveloper is a extended cluster role for namespace developer
var SchemaExtNamespaceDeveloper = &testv1.ClusterRole{
	TypeMeta: metav1.TypeMeta{
		Kind:       "ClusterRole",
		APIVersion: "rbac.authorization.k8s.io/v1",
	},
	ObjectMeta: metav1.ObjectMeta{
		Name: constClusterrole.EXT_NAMESPACE_DEVELOPER,
	},
	Rules: []testv1.PolicyRule{
		{
			APIGroups: []string{
				"",
			},
			Resources: []string{
				"namespaces",
				"events",
			},
			Verbs: []string{
				// "get",
				"list",
			},
		},
		{
			APIGroups: []string{
				"route.openshift.io",
			},
			Resources: []string{
				"*",
			},
			Verbs: []string{
				"*",
			},
		},
		// add list storageclass permission
		extListStorageClassPermission,
		// portal start
		{
			APIGroups: []string{
				"",
			},
			Resources: []string{
				"configmaps",
			},
			ResourceNames: []string{
				"portal-configmap",
			},
			Verbs: []string{
				"get",
			},
		},
		{
			APIGroups: []string{"portal.alauda.io"},
			Resources: []string{"alaudaproducts"},
			Verbs:     []string{"list"},
		},
		{
			APIGroups:     []string{"portal.alauda.io"},
			Resources:     []string{"alaudaproducts"},
			Verbs:         []string{"get"},
			ResourceNames: OtherPortalProducts,
		},
		// portal end
		// devops start
		// project: change in the future
		extListProjectsPermission,
		extListAuthProjectsPermission,
		extListGetWatchCatalogPermission,
		extListGetListWatchEventsPermission,
		{
			APIGroups: []string{
				groups.DevOpsAPIGroup,
			},
			Resources: []string{
				"clusterpipelinetemplates",
				"clusterpipelinetasktemplates",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		{
			APIGroups: []string{
				groups.AUTH_APIGROUP,
			},
			Resources: []string{
				"clusterpipelinetemplates",
				"clusterpipelinetasktemplates",
			},
			Verbs: []string{
				"get",
				"list",
				"watch",
			},
		},
		{
			APIGroups: []string{
				groups.DevOpsAPIGroup,
			},
			Resources: []string{
				"clusterpipelinetemplates/preview",
			},
			Verbs: []string{
				"get",
				"create",
			},
		},
		{
			APIGroups: []string{
				groups.AUTH_APIGROUP,
			},
			Resources: []string{
				"clusterpipelinetemplates/preview",
			},
			Verbs: []string{
				"get",
				"create",
			},
		},
		// devops end
		// catalog

		// asf
		{
			APIGroups: []string{groups.ASFAPIGroup},
			Resources: []string{"*"},
			Verbs:     []string{"get", "list", "watch"},
		},
		// asm start
		extListGetWatchAsmClusterConfigPermission,
		// asm end
	},
}
