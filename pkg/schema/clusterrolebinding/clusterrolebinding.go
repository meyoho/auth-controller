package clusterrolebinding

import (
	"alauda.io/auth-controller/pkg/constant/clusterrole"
	testv1 "k8s.io/api/rbac/v1"
)

// ClusterRoleBindingMap is a map between the ClusterRole name and the extended ClusterRoleBinding
var ClusterRoleBindingMap = map[string]*testv1.ClusterRoleBinding{
	clusterrole.PROJECT_ADMIN:       SchemaExtProjectAdmin,
	clusterrole.PROJECT_AUDITOR:     SchemaExtProjectAuditor,
	clusterrole.NAMESPACE_DEVELOPER: SchemaExtNamespaceDeveloper,
	clusterrole.NAMESPACE_ADMIN:     SchemaExtNamespaceAdmin,
	clusterrole.NAMESPACE_AUDITOR:   SchemaExtNamespaceAuditor,
}
