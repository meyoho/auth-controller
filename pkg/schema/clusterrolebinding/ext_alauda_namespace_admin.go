package clusterrolebinding

import (
	constClusterrole "alauda.io/auth-controller/pkg/constant/clusterrole"
	"alauda.io/auth-controller/pkg/constant/label"
	testv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var SchemaExtNamespaceAdmin = &testv1.ClusterRoleBinding{
	TypeMeta: metav1.TypeMeta{
		Kind:       "ClusterRoleBinding",
		APIVersion: "rbac.authorization.k8s.io/v1",
	},
	ObjectMeta: metav1.ObjectMeta{
		// to be replaced
		Name: "<clusterrolebindingname>",
		Labels: map[string]string{
			label.SYSTEM_CLUSTERROLEBINDING: "true",
		},
	},
	RoleRef: testv1.RoleRef{
		APIGroup: "rbac.authorization.k8s.io",
		Kind:     "ClusterRole",
		Name:     constClusterrole.EXT_NAMESPACE_ADMIN,
	},
	Subjects: []testv1.Subject{
		{
			Kind: "User",
			// to be replaced
			Name:     "<username>",
			APIGroup: "rbac.authorization.k8s.io",
		},
	},
}
