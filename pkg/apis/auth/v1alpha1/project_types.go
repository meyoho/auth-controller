/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ProjectSpec defines the desired state of Project
type ProjectSpec struct {
	// Namespaces is a list of ProjectNamespace objects.
	// +optional
	Namespaces []ProjectNamespace `json:"namespaces"`
}

// ProjectNamespace defines a namespace inside ProjectSpec
type ProjectNamespace struct {
	// Name of the namespace in project.
	// +optional
	Name string `json:"name"`
	// Type of the namespace in project.
	// +optional
	Type string `json:"type"`
}

// ProjectStatus defines the observed state of Project
type ProjectStatus struct {
	// Namespaces is a list of ProjectNamespace objects.
	// +optional
	Namespaces []ProjectNamespaceStatus `json:"namespaces"`
}

// ProjectNamespaceStatus defines a Project Namespace relationship status
type ProjectNamespaceStatus struct {
	// Name of the namespace in project.
	// +optional
	Name string `json:"name"`
	// Status of the namespace in project.
	// +optional
	Status string `json:"status"`
}

// +genclient
// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Project is the Schema for the projects API
// +k8s:openapi-gen=true
type Project struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ProjectSpec   `json:"spec,omitempty"`
	Status ProjectStatus `json:"status,omitempty"`
}

// +genclient:nonNamespaced
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ProjectList contains a list of Project
type ProjectList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Project `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Project{}, &ProjectList{})
}
