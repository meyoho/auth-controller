package v1alpha1

import (
	"time"

	corev1 "k8s.io/api/core/v1"
)

const (
	// ProductName product name
	ProductName = "Alauda DevOps"
	// ProjectNamespaceType is a relationship type between both resources
	// currently only "owned" is supported

	// ProjectNamespaceTypeOwned ownership type of relationship
	ProjectNamespaceTypeOwned = "owned"
	// ProjectNamespaceStatusUnknown status unknown
	ProjectNamespaceStatusUnknown = "Unknown"
	// ProjectNamespaceStatusReady status ready
	ProjectNamespaceStatusReady = "Ready"
	// ProjectNamespaceStatusError status error
	ProjectNamespaceStatusError = "Error"

	// StatusCreating creating
	StatusCreating = "Creating"
	// StatusReady can be reached
	StatusReady = "Ready"
	// StatusError service cannot be reached
	StatusError = "Error"
	// StatusSyncing Syncing
	StatusSyncing = "Syncing"
	// StatusDisabled Disabled
	StatusDisabled = "Disabled"
	//StatusWaitingToDelete WaitingToDelete
	StatusWaitingToDelete = "WaitingToDelete"
	//StatusListRepoError ListRepoError
	StatusListRepoError = "ListRepoError"
	//StatusListTagError ListTagError
	StatusListTagError = "ListTagError"

	// JenkinsBindingStatus has a slice of Conditions (like pods)
	// that are used to store different parts of required data
	// like Jenkins and Secret

	// JenkinsBindingStatusTypeJenkins condition type for Jenkinsbinding
	JenkinsBindingStatusTypeJenkins = "Jenkins"
	// JenkinsBindingStatusTypeSecret condition type for Secret
	JenkinsBindingStatusTypeSecret = "Secret"
	// JenkinsBindingStatusTypeRepository condition type for Secret
	JenkinsBindingStatusTypeRepository = "CodeRepository"
	// JenkinsBindingStatusTypeImageRepository condition type for ImageRepository
	JenkinsBindingStatusTypeImageRepository = "ImageRepository"

	// For each condition type we have a list of valid reasons:

	// JenkinsBindingStatusConditionStatusNotFound when a condition type is NotFound
	JenkinsBindingStatusConditionStatusNotFound = "NotFound"
	// JenkinsBindingStatusConditionStatusNotValid when a condition type is not filled
	JenkinsBindingStatusConditionStatusNotValid = "NotValid"
	// JenkinsBindingStatusConditionStatusReady when a condition type is Found
	JenkinsBindingStatusConditionStatusReady = "Ready"

	// RBAC related

	// RoleNameDeveloper  Role for developer-tester
	RoleNameDeveloper = "devops-developer"
	// RoleDisplayNameDeveloper display name for role developer
	RoleDisplayNameDeveloper = "开发测试"
	// RoleDisplayNameDeveloperEn english display name for role developer
	RoleDisplayNameDeveloperEn = "Developer"
	// RoleNameProjectManager Role for project manager
	RoleNameProjectManager = "devops-project-manager"
	// RoleDisplayNameProjectManager display name for ProjectManager
	RoleDisplayNameProjectManager = "项目经理"
	// RoleDisplayNameProjectManagerEn english display name for ProjectManager
	RoleDisplayNameProjectManagerEn = "Project Manager"
	// RoleNamePlatformAdmin ClusterRole for platform admin
	RoleNamePlatformAdmin = "devops-platform-admin"
	// RoleDisplayNamePlatformAdmin display name for Platform Admin
	RoleDisplayNamePlatformAdmin = "平台管理员"
	// RoleDisplayNamePlatformAdminEn english display name for PlatformAdmin
	RoleDisplayNamePlatformAdminEn = "Platform Administrator"
	// RoleNameProjectUser  Role for user
	RoleNameProjectUser = "devops-project-user"
	// RoleDisplayNameProjectUser display name for role project-user
	RoleDisplayNameProjectUser = "项目成员"
	// RoleDisplayNameProjectUserEn english display name for role project-user
	RoleDisplayNameProjectUserEn = "Project Member"

	// region Annotation: Used as key in Annotations

	// AnnotationsKeyDisplayName displayName key for annotations
	AnnotationsKeyDisplayName = "alauda.io/displayName"
	// AnnotationsKeyDisplayNameEn english displayName key for annotations
	AnnotationsKeyDisplayNameEn = "alauda.io/displayNameEn"
	// AnnotationsKeyProduct product name key for annotations
	AnnotationsKeyProduct = "alauda.io/product"
	// AnnotationsKeyProductVersion product version key for annotations
	AnnotationsKeyProductVersion = "alauda.io/productVersion"
	// AnnotationsKeyRoleVersion role version key for annotations
	AnnotationsKeyRoleVersion = "alauda.io/roleVersion"
	// AnnotationsKeyProject project key for annotations
	AnnotationsKeyProject = "alauda.io/project"
	// AnnotationsKeyPipelineLastNumber last number used to generate pipeline store in PipelineConfig
	AnnotationsKeyPipelineLastNumber = "alauda.io/pipeline.last.number"
	// AnnotationsKeyPipelineNumber key for specific pipeline Number
	AnnotationsKeyPipelineNumber = "alauda.io/pipeline.number"
	// AnnotationsKeyPipelineConfig key for specific pipeline config
	AnnotationsKeyPipelineConfig = "alauda.io/pipelineConfig.name"
	// AnnotationsJenkinsBuildURI annotations key that contains the build uri
	AnnotationsJenkinsBuildURI = "alauda.io/jenkins-build-uri"
	// AnnotationsSecretType annotations key for specific secret type
	AnnotationsSecretType = "alauda.io/secretType"
	// AnnotationsCreateAppUrl annotations key for specific create app url
	AnnotationsCreateAppUrl = "alauda.io/createAppUrl"

	// AnnotationsKeyServiceMeshInjection annotations key for service mesh enable or disable
	AnnotationsKeyServiceMeshInjection = "alauda.io/serviceMesh"

	// endregion

	// region Type: Pascal nomenclature
	TypeProject                  = "Project"
	TypeNamespace                = "Namespace"
	TypeJenkins                  = "Jenkins"
	TypeJenkinsBinding           = "JenkinsBinding"
	TypePipelineConfig           = "PipelineConfig"
	TypePipeline                 = "Pipeline"
	TypeCodeRepoService          = "CodeRepoService"
	TypeCodeRepoBinding          = "CodeRepoBinding"
	TypeCodeRepository           = "CodeRepository"
	TypeImageRegistry            = "ImageRegistry"
	TypeImageRegistryBinding     = "ImageRegistryBinding"
	TypeImageRepository          = "ImageRepository"
	TypeProjectManagement        = "ProjectManagement"
	TypeProjectManagementBinding = "ProjectManagementBinding"
	TypeTestTool                 = "TestTool"
	TypeTestToolBinding          = "TestToolBinding"

	// endregion

	// region Kind: all words were lower case
	ResourceKindProject                  = "project"
	ResourceKindJenkins                  = "jenkins"
	ResourceKindJenkinsBinding           = "jenkinsbinding"
	ResourceKindPipelineConfig           = "pipelineconfig"
	ResourceKindPipeline                 = "pipeline"
	ResourceKindCodeRepoService          = "codereposervice"
	ResourceKindCodeRepoBinding          = "coderepobinding"
	ResourceKindCodeRepository           = "coderepository"
	ResourceKindImageRegistry            = "imageregistry"
	ResourceKindImageRegistryBinding     = "imageregistrybinding"
	ResourceKindImageRepository          = "imagerepository"
	ResourceKindProjectManagement        = "projectmanagement"
	ResourceKindProjectManagementBinding = "projectmanagementbinding"
	ResourceKindTestTool                 = "testtool"
	ResourceKindTestToolBinding          = "testtoolbinding"
	ResourceKindCodeQualityTool          = "codequalitytool"
	ResourceKindCodeQualityToolBinding   = "codequalitytoolbinding"
	// endregion

	// region Label: Camel nomenclature, used as the key in labels or other place
	LabelProject                  = ResourceKindProject
	LabelJenkins                  = ResourceKindJenkins
	LabelJenkinsBinding           = "jenkinsBinding"
	LabelPipelineConfig           = "pipelineConfig"
	LabelPipeline                 = ResourceKindPipeline
	LabelCodeRepoService          = "codeRepoService"
	LabelCodeRepoBinding          = "codeRepoBinding"
	LabelCodeRepository           = "codeRepository"
	LabelProjectManagement        = "projectManagement"
	LabelProjectManagementBinding = "projectManagementBinding"
	LabelTestTool                 = "testTool"
	LabelTestToolBinding          = "testToolBinding"
	LabelImageRegistry            = "imageRegistry"
	LabelImageRegistryBinding     = "imageRegistryBinding"
	LabelImageRepository          = "imageRepository"

	// LabelDevopsAlaudaIOKey key used for specific Labels
	LabelDevopsAlaudaIOKey = "devops.alauda.io"
	// LabelDevopsAlaudaIOProjectKey key used for roles that are using in a project
	LabelDevopsAlaudaIOProjectKey = "devops.alauda.io/project"
	// LabelCodeRepoServiceType label key for codeRepoServiceType
	LabelCodeRepoServiceType = "codeRepoServiceType"
	// LabelCodeRepoServicePublic label key for codeRepoServicePublic
	LabelCodeRepoServicePublic = "codeRepoServicePublic"
	// LabelImageRegistryType label key for imageRegistryType
	LabelImageRegistryType = "imageRegistryType"
	// LabelImageRegistryEndpoint label key for imageRegistryEndpoint
	LabelImageRegistryEndpoint = "imageRegistryEndpoint"
	// LabelDevopsAlaudaIOGlobalKey key used for global secret
	LabelDevopsAlaudaIOGlobalKey = "devops.alauda.io/global"

	// LabelistioInjectionKey key used for istio injection
	LabelIstioInjectionKey = "istio-injection"

	// endregion

	// region ToolChainItem
	// configmap
	ConfigMapKindName        = "ConfigMap"
	ConfigMapAPIVersion      = "v1"
	SettingsConfigMapName    = "devops-config"
	SettingsKeyDomain        = "_domain"
	SettingsKeyGithubCreated = "githubCreated"
	SettingsKeyToolChains    = "toolChains"

	// github
	GithubName          = "github"
	GithubHost          = "https://api.github.com"
	GithubHTML          = "https://github.com"
	GithubDisplayNameCN = "Github"
	GithubDisplayNameEN = "Github"

	// gitlab
	GitlabName          = "gitlab"
	GitlabHost          = "https://gitlab.com"
	GitlabHTML          = "https://gitlab.com"
	GitlabDisplayNameCN = "Gitlab"
	GitlabDisplayNameEN = "Gitlab"

	// gitlab-private
	GitlabPrivateName          = "gitlab-enterprise"
	GitlabPrivateHost          = ""
	GitlabPrivateHTML          = ""
	GitlabPrivateDisplayNameCN = "Gitlab 企业版"
	GitlabPrivateDisplayNameEN = "Gitlab Enterprise"

	// gitee
	GiteeName          = "gitee"
	GiteeHost          = "https://gitee.com"
	GiteeHTML          = "https://gitee.com"
	GiteeDisplayNameCN = "码云"
	GiteeDisplayNameEN = "Gitee"

	// gitee-private
	GiteePrivateName          = "gitee-enterprise"
	GiteePrivateHost          = ""
	GiteePrivateHTML          = ""
	GiteePrivateDisplayNameCN = "码云企业版"
	GiteePrivateDisplayNameEN = "Gitee Enterprise"

	// bitbucket
	BitbucketName          = "bitbucket"
	BitbucketHost          = "https://api.bitbucket.org"
	BitbucketHTML          = "https://bitbucket.org"
	BitbucketDisplayNameCN = "Bitbucket"
	BitbucketDisplayNameEN = "Bitbucket"

	// jira
	JiraName          = "jira"
	JiraDisplayNameCN = "Jira"
	JiraDisplayNameEN = "Jira"

	// taiga
	TaigaName          = "taiga"
	TaigaDisplayNameCN = "Taiga"
	TaigaDisplayNameEN = "Taiga"

	// redwoodhq
	RedwoodHQName          = "redwoodhq"
	RedwoodHQDisplayNameCN = "RedwoodHQ"
	RedwoodHQDisplayNameEN = "RedwoodHQ"

	// docker registry
	DockerRegistryName          = "docker-registry"
	DockerRegistryDisplayNameCN = "Docker Registry"
	DockerRegistryDisplayNameEN = "Docker Registry"

	// harbor registry
	HarborRegistryName          = "harbor-registry"
	HarborRegistryDisplayNameCN = "Harbor Registry"
	HarborRegistryDisplayNameEN = "Harbor Registry"

	// jenkins
	JenkinsName          = "jenkins"
	JenkinsDisplayNameCN = "Jenkins"
	JenkinsDisplayNameEN = "Jenkins"

	// sonarqube
	SonarQubeName          = "sonarqube"
	SonarQubeDisplayNameCN = "SonarQube"
	SonarQubeDisplayNameEN = "SonarQube"

	// endregion

	// region ToolChainElement

	// codeRepository
	ToolChainCodeRepositoryName          = "codeRepository"
	ToolChainCodeRepositoryDisplayNameCN = "代码仓库"
	ToolChainCodeRepositoryDisplayNameEN = "Code Repository"

	// continuousIntegration
	ToolChainContinuousIntegrationName          = "continuousIntegration"
	ToolChainContinuousIntegrationDisplayNameCN = "持续集成"
	ToolChainContinuousIntegrationDisplayNameEN = "Continuous Integration"

	// artifactRepository
	ToolChainArtifactRepositoryName          = "artifactRepository"
	ToolChainArtifactRepositoryDisplayNameCN = "制品仓库"
	ToolChainArtifactRepositoryDisplayNameEN = "Artifact Repository"

	// testTool
	ToolChainTestToolName          = "testTool"
	ToolChainTestToolDisplayNameCN = "测试工具"
	ToolChainTestToolDisplayNameEN = "Test tool"

	// projectManagement
	ToolChainProjectManagementName          = "projectManagement"
	ToolChainProjectManagementDisplayNameCN = "项目管理"
	ToolChainProjectManagementDisplayNameEN = "Project Management"

	// codeQualityTool
	ToolChainCodeQualityToolName          = "codeQualityTool"
	ToolChainCodeQualityToolDisplayNameCN = "代码检查工具"
	ToolChainCodeQualityToolDisplayNameEN = "Code Quality Tool"

	// endregion

	// TrueString true as string
	TrueString = "true"

	// APIVersionV1Alpha1 version of the api
	APIVersionV1Alpha1 = "devops.alauda.io/v1alpha1"

	// region TTL
	TTLSession              = 1 * time.Minute
	TTLCheckCodeRepoService = 5 * time.Minute
	TTLCheckCodeRepoBinding = 5 * time.Minute
	TTLCheckCodeRepository  = 5 * time.Minute
	// endregion

	// imageRegistry
	SettingsKeyImageRegistryTypes = "imageRegistryTypes"
	SettingsKeyDockerCreated      = "dockerCreated"

	// region OAuth2
	// SecretTypeOAuth2 contains data needed for oauth2 authentication.
	//
	// Required fields:
	// - Secret.Data["clientID"] - client id used for authentication
	// - Secret.Data["clientSecret"] - client secret used for authentication
	SecretTypeOAuth2 corev1.SecretType = "devops.alauda.io/oauth2"

	// OAuth2ClientIDKey is the key of the clientID for SecretTypeOAuth2 secrets
	OAuth2ClientIDKey = "clientID"
	// OAuth2ClientSecretKey is the key of the clientSecret for SecretTypeOAuth2 secrets
	OAuth2ClientSecretKey = "clientSecret"
	// OAuth2CodeKey is the key of the code for SecretTypeOAuth2 secrets
	OAuth2CodeKey = "code"
	// OAuth2AccessTokenKeyKey is the key of the accessTokenKey for SecretTypeOAuth2 secrets
	OAuth2AccessTokenKeyKey = "accessTokenKey"
	// OAuth2AccessTokenKey is the key of the accessToken for SecretTypeOAuth2 secrets
	OAuth2AccessTokenKey = "accessToken"
	// OAuth2ScopeKey is the key of the scope for SecretTypeOAuth2 secrets
	OAuth2ScopeKey = "scope"
	// OAuth2RefreshTokenKey is the key of the refreshToken for SecretTypeOAuth2 secrets
	OAuth2RefreshTokenKey = "refreshToken"
	// OAuth2ExpiresInKey is the key of the expiresIn for SecretTypeOAuth2 secrets
	OAuth2CreatedAtKey = "createdAt"
	// OAuth2ExpiresInKey is the key of the expiresIn for SecretTypeOAuth2 secrets
	OAuth2ExpiresInKey = "expiresIn"
	// endregion

	NamespaceAlaudaSystem      = "alauda-system"
	NamespaceDefault           = "default"
	NamespaceKubeSystem        = "kube-system"
	NamespaceGlobalCredentials = "global-credentials"
)
