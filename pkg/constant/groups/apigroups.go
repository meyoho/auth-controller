package groups

const (
	// DevOpsAPIGroup api group for DevOps resources
	DevOpsAPIGroup = "devops.alauda.io"
	// CatalogAPIGroup api group for catalog resources
	CatalogAPIGroup = "catalog.alauda.io"
	// EventsAPIGroup api group for events resources
	EventsAPIGroup = "events.k8s.io"
	// ASFAPIGroup api group for asf resources
	ASFAPIGroup = "asf.alauda.io"
	// AppAPIGroup app api group
	AppAPIGroup = "app.k8s.io"

	// AUTH_APIGROUP api group for DevOps resources
	AUTH_APIGROUP = "auth.alauda.io"

	IstioNetworkApiGroup = "networking.istio.io"

	IstioAuthApiGroup = "authentication.istio.io"
	AsmAPIGroup          = "asm.alauda.io"
)
