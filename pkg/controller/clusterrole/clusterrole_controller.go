/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package clusterrole

import (
	"alauda.io/auth-controller/pkg/schema/clusterrole"
	"context"
	testv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/uuid"
	"log"
	"reflect"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new Role Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
// USER ACTION REQUIRED: update cmd/manager/main.go to call this test.Add(mgr) to install this Controller
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileClusterRole{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// InitDefaultClusterRoles init the default cluster roles
func InitDefaultClusterRoles(k8sClient client.Client) error {
	for roleName, clusterRole := range clusterrole.DefaultClusterRoles {
		log.Printf("InitDefaultClusterRoles clusterrole name: <%s>\n", roleName)

		instance := clusterRole.DeepCopy()
		err := k8sClient.Delete(context.TODO(), instance)
		if err != nil {
			log.Printf("InitDefaultClusterRoles [error] error occur at deleting clusterrole: <%s>, error: %v\n", roleName, err)
		} else {
			log.Printf("InitDefaultClusterRoles [event] deleted clusterrole <%s>\n", roleName)
		}

		err = k8sClient.Create(context.TODO(), instance)
		if err != nil {
			if errors.IsAlreadyExists(err) {
				log.Printf("InitDefaultClusterRoles clusterrole <%s> exists, go to next...\n", roleName)
				continue
			} else {
				return err
			}
		} else {
			log.Printf("InitDefaultClusterRoles [event] created clusterrole <%s>\n", roleName)
		}
	}
	return nil
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("clusterrole-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to Role
	err = c.Watch(&source.Kind{Type: &testv1.ClusterRole{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileClusterRole{}

// ReconcileRole reconciles a Role object
type ReconcileClusterRole struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a Role object and makes changes based on the state read
// and what is in the Role.Spec
// a Deployment as an example
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=test.alauda.io,resources=clusterroles,verbs=get;list;watch;create;update;patch;delete
func (r *ReconcileClusterRole) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	requestUuid := string(uuid.NewUUID())
	log.Printf("%s ReconcileClusterRole request name: %s\n", requestUuid, request.Name)

	// Fetch the ClusterRole instance
	instance := &testv1.ClusterRole{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Handle deleted event
			if clusterRoleSchema, ok := clusterrole.DefaultClusterRoles[request.Name]; ok {
				log.Printf("%s ReconcileClusterRole [event] creating %s\n", requestUuid, request.Name)
				err = r.Create(context.TODO(), clusterRoleSchema.DeepCopy())
				if err != nil {
					return reconcile.Result{}, err
				}
			}
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	//log.Printf("%s ReconcileClusterRole find instance name: %s\n", requestUuid, instance.Name)

	// Handle update event
	if clusterRoleSchema, ok := clusterrole.DefaultClusterRoles[instance.Name]; ok {
		// Update the found object and write the result back if there are any changes
		if !reflect.DeepEqual(clusterRoleSchema.Rules, instance.Rules) {
			instance.Rules = clusterRoleSchema.Rules
			log.Printf("%s ReconcileClusterRole [event] updating %s\n", requestUuid, instance.Name)
			err = r.Update(context.TODO(), instance)
			if err != nil {
				return reconcile.Result{}, err
			}
		}
	}

	return reconcile.Result{}, nil
}
