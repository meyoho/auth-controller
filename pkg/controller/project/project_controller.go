/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package project

import (
	"context"
	"log"

	"alauda.io/auth-controller/pkg/apis"
	authv1beta1 "alauda.io/auth-controller/pkg/apis/auth/v1alpha1"
	"github.com/golang/glog"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new Project Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
// USER ACTION REQUIRED: update cmd/manager/main.go to call this auth.Add(mgr) to install this Controller
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

func InitDefaultProject(k8sClient client.Client) error {
	projectResource := &v1beta1.CustomResourceDefinition{
		TypeMeta: metav1.TypeMeta{
			Kind:       "CustomResourceDefinition",
			APIVersion: "apiextensions.k8s.io/v1beta1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: "projects.auth.alauda.io",
		},
		Spec: v1beta1.CustomResourceDefinitionSpec{
			Group:   "auth.alauda.io",
			Version: "v1alpha1",
			Names: v1beta1.CustomResourceDefinitionNames{
				Plural:   "projects",
				Singular: "project",
				Kind:     "Project",
				ListKind: "ProjectList",
			},
			Scope: v1beta1.ResourceScope("Cluster"),
		},
	}

	resourceObject := apis.ResourceObject{
		Name:   "Project",
		Object: projectResource,
	}

	log.Printf("create projects crd resource object completed.")

	err := apis.InitResource(k8sClient, resourceObject)

	if err != nil {
		log.Println(err)
		return err
	}

	log.Printf("init projects crd completed.")

	return nil
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileProject{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("project-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to Project
	err = c.Watch(&source.Kind{Type: &authv1beta1.Project{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// TODO(user): Modify this to be the types you create
	// Uncomment watch a Deployment created by Project - change this for objects you create
	err = c.Watch(&source.Kind{Type: &corev1.Namespace{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &authv1beta1.Project{},
	})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileProject{}

// ReconcileProject reconciles a Project object
type ReconcileProject struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a Project object and makes changes based on the state read
// and what is in the Project.Spec
// TODO(user): Modify this Reconcile function to implement your Controller logic.  The scaffolding writes
// a Deployment as an example
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=auth.alauda.io,resources=projects,verbs=get;list;watch;create;update;patch;delete
func (r *ReconcileProject) Reconcile(request reconcile.Request) (reconcile.Result, error) {

	err := r.syncHandler(request)

	return reconcile.Result{}, err
}

// newNamespace creates a new Namespace for a Project resource. It also sets
// the appropriate OwnerReferences on the resource so handleObject can discover
// the Project resource that 'owns' it.
func newNamespace(project *authv1beta1.Project) *corev1.Namespace {
	labels := map[string]string{
		authv1beta1.AnnotationsKeyProject: project.Name,
	}
	// service mesh injection
	if val, ok := serviceMesh(project); ok {
		labels[authv1beta1.LabelIstioInjectionKey] = val
	}

	return &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:   project.Name,
			Labels: labels,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(project, schema.GroupVersionKind{
					Group:   authv1beta1.SchemeGroupVersion.Group,
					Version: authv1beta1.SchemeGroupVersion.Version,
					Kind:    "Project",
				}),
			},
		},
	}
}

func (r *ReconcileProject) updateProjectSpec(proj *authv1beta1.Project) (err error) {
	// proj must be a copy of the original and modified
	r.Update(context.TODO(), proj)
	return
}

func (r *ReconcileProject) syncHandler(request reconcile.Request) error {

	// Convert the namespace/name string into a distinct namespace and name
	nameString := request.Name
	//namespaceString:=request.Namespace
	//if namespaceString=="" {
	//	namespaceString="default"
	//}

	glog.V(4).Infof("syncing project/namespace: name: %v", nameString)

	// get the namespace object first
	namespace := &corev1.Namespace{}
	getNamespaceErr := r.Get(context.TODO(), types.NamespacedName{Name: request.Name}, namespace)

	glog.V(4).Infof("got project instance")

	//get the project object
	project := &authv1beta1.Project{}

	getProjectErr := r.Get(context.TODO(), types.NamespacedName{Name: request.Name}, project)

	if getProjectErr == nil && getNamespaceErr == nil {
		if namespace.Status.Phase == "Terminating" {

			//wait namespace delete
			glog.V(4).Infof("project %v exist,and namespace terminating", request.Name)
			go r.createNamespace(project, namespace)
		} else {

			//project exist && namespace exist,this is nomal case,safe leave.
			glog.V(4).Infof("project %v normal exit", request.Name)

			r.serviceMeshInjection(project, namespace)

			return nil
		}

	} else if getProjectErr == nil && getNamespaceErr != nil && errors.IsNotFound(getNamespaceErr) {

		//project exist but namespace not found.In this case,need create namespace named the same as project
		glog.V(4).Infof("project %v exist,but namespace not found", request.Name)
		go r.createNamespace(project, namespace)
	} else if getProjectErr != nil && errors.IsNotFound(getProjectErr) {

		//project not found ,nothing to do,safe leave.
		glog.V(4).Infof("project %v not found", request.Name)
		return nil
	}

	glog.V(4).Infof("project %v exit", request.Name)
	return nil
}

func (r *ReconcileProject) createNamespace(project *authv1beta1.Project, namespace *corev1.Namespace) {

	// NEVER modify objects from the store. It's a read-only, local cache.
	// You can use DeepCopy() to make a deep copy of original object and modify this copy
	// Or create a copy manually for better performance
	projCopy := project.DeepCopy()
	//needsEvent := false

	// this is to enforce our current limitation of 1 project to 1 namespace
	// we need to make sure the data is correct and fix if necessary
	// TODO: change this rule once the project supports more than one namespace
	if len(project.Spec.Namespaces) != 1 {
		glog.V(4).Infof("project spec is invalid. Will fix it..")

		//TODO
		//c.recorder.Event(project, corev1.EventTypeWarning, ErrProjectSpecInvalid, MessageProjectSpecInvalid)

		correct := authv1beta1.ProjectNamespace{
			Name: project.GetName(),
			Type: authv1beta1.ProjectNamespaceTypeOwned,
		}
		if len(project.Spec.Namespaces) == 0 {
			projCopy.Spec.Namespaces = []authv1beta1.ProjectNamespace{correct}
		} else {
			projCopy.Spec.Namespaces = project.Spec.Namespaces[:1]
			projCopy.Spec.Namespaces[0] = correct
		}
		//needsEvent = true
		// TODO: Once the status subresource is ready
		// we should update the project, return
		// and let the loop start again
	}

	if projCopy.Status.Namespaces == nil {
		projCopy.Status.Namespaces = []authv1beta1.ProjectNamespaceStatus{}
	}

	//var namespaceObj *corev1.Namespace

	// loop through project getting namespace status
	for _, n := range projCopy.Spec.Namespaces {
		found := -1
		for i, s := range projCopy.Status.Namespaces {
			if s.Name == n.Name {
				found = i
				break
			}
		}
		// not found, should add one
		if found == -1 {
			glog.V(4).Infof("project status does not contain namespace %v, will add now", n.Name)
			projCopy.Status.Namespaces = append(projCopy.Status.Namespaces, authv1beta1.ProjectNamespaceStatus{
				Name:   n.Name,
				Status: authv1beta1.ProjectNamespaceStatusUnknown,
			})
			found = len(projCopy.Status.Namespaces) - 1
		}
		// get the namespace object first

		namespace := &corev1.Namespace{}
		getNamespaceErr := r.Get(context.TODO(), types.NamespacedName{Name: project.Name}, namespace)
		if getNamespaceErr != nil && errors.IsNotFound(getNamespaceErr) {
			//needsEvent = true
			glog.V(4).Infof("Namespace %v does not exist. Will create now", n.Name)

			//TODO re
			createNamespaceErr := r.Create(context.TODO(), newNamespace(project))

			//create success
			if createNamespaceErr == nil {

				projCopy.Status.Namespaces[found].Status = authv1beta1.ProjectNamespaceStatusReady
				glog.V(4).Infof("Namespace %v create successful", n.Name)
			} else {
				glog.Errorf("Namespace %v create error: %v", project.Name, createNamespaceErr)

				projCopy.Status.Namespaces[found].Status = authv1beta1.ProjectNamespaceStatusError
			}
		} else if getNamespaceErr == nil && namespace.Name == project.Name {
			// namespace exists
			if projCopy.Status.Namespaces[found].Status != authv1beta1.ProjectNamespaceStatusReady {
				//needsEvent = true
				projCopy.Status.Namespaces[found].Status = authv1beta1.ProjectNamespaceStatusReady
			}
		}
	}
}

func serviceMesh(project *authv1beta1.Project) (string, bool) {
	annotations := project.Annotations
	if annotations == nil {
		annotations = map[string]string{}
	}
	if val, ok := annotations[authv1beta1.AnnotationsKeyServiceMeshInjection]; ok {

		return val, true
	}

	return "", false
}

func (r *ReconcileProject) serviceMeshInjection(project *authv1beta1.Project, namespace *corev1.Namespace) {

	labels := namespace.GetLabels()
	namespaceCopy := namespace.DeepCopy()

	if labels == nil {
		labels = map[string]string{}
	}

	if val, ok := serviceMesh(project); ok {

		labels[authv1beta1.LabelIstioInjectionKey] = val

	} else {
		delete(labels, authv1beta1.LabelIstioInjectionKey)

	}

	namespaceCopy.SetLabels(labels)

	r.Update(context.TODO(), namespaceCopy)

}
